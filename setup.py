from setuptools import setup, find_packages
import os


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join(path, filename)[22:])
    return paths

setup(name='turbudata',
      version='0.1',
      description='A small collection of CFD datasets.',
      url='https://bitbucket.org/lesituu/turbudata',

      author='Timofey Mukha',
      author_email='timofey.mukha@it.uu.se',
      packages=find_packages(),
      install_requires=[
                        'numpy',
                        'h5py',
                       ],
      license="GNU GPL 3",
      classifiers=[
          "Development Status :: 4 - Beta",
      ],
      zip_safe=False)

