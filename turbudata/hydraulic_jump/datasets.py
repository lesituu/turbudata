from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
import numpy as np

__all__ = ["dns_mortazavi"]


def dns_mortazavi():
    """
    Returns the path to the DNS dataset by Mortazavi at al.
    """

    return os.path.join(os.path.dirname(sys.modules[__name__].__file__),
                        "datasets_files", "hydraulic_jump_dns_mortazavi.hdf5")


