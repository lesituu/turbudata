# README #

This repository hosts a small python package -- `turbudata`.
The package is a small collection of CFD datasets, which were converted into HDF5 format for conveniece.
Function for accessing the datasets are provided.

## Installing ##
Install as by running *python setup.py develop* in the root of the repository.

## Using
You must have the `h5py` package installed to use `turbudata`. Import both in your python script.
All datasets are accessed by creating calling `h5py.File(path_to_dataset)`. The package provides functions that return the path to the datasets (i.e. the argument for `h5py.File`).
To see what is inside the dataset you can use the `.keys()` function of the `h5py.File`. So is variable `f` holds the File, run `print(list(f.keys))`.

## Datasets ##

### Channel flow ###

 * DNS by Lee and Moser, `turbudata.channel_flow.dns_lee_moser()`.
 
 * DNS by Moser, Kim and Mansour, `turbudata.channel_flow.dns_moser_kim_mansour()`.
 
 * DNS by Jeminez, `turbudata.channel_flow.dns_jimenez()`
 
### ZPGTBL ###

 * DNS by Schlatter, `turbudata.zpgtbl.dns_schlatter()`

### BFS ###

 * DNS by Le and Moin, `turbudata.bfs.dns_le_moin()`.
 
 * Experimental data by Jovic and Driver, `turbudata.bfs.jovic_driver()`
 
## Example ##

Let us plot u+ vs y+ for the Re_tau=5200 channel flow DNS of Lee and Moser.

~~~~
import h5py
from turbudata import *
import matplotlib.pyplot as plt

f = h5py.File(channel_flow.dns_lee_moser())
plt.plot(f["5200"]["yPlus"], f["5200"]["uPlus"])
plt.show()
~~~~
